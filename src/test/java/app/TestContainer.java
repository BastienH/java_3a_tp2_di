package app;

import app.classes.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 *      +---> B
 * A ---|         +---> F ---> G
 *      +---> E --+
 *
 *      +-- C
 * B <--|
 *      +-- D
 */

public class TestContainer
{
    @BeforeEach
    void createContainer()
    {
        Container.register(B.class, D.class);
        Container.register(E.class, E.class);
        Container.register(F.class, F.class);
        Container.register(G.class, G.class);
    }

    @Test
    @DisplayName("Test valid register")
    void testValidRegister()
    {

        assertEquals(4, Container._dependencies.size());
    }

    @Test
    @DisplayName("Test fail register")
    void testFailRegister()
    {
        Container.register(B.class, List.class);

        assertEquals(4, Container._dependencies.size());
    }


    @Test
    @DisplayName("Test new instance with setters")
    void testNewInstanceWithSetters()
    {
        A success = Container.newInstanceWithSetters(A.class);

        assertNotNull(success);
        assertTrue(success.getB_public() instanceof D);


        B fail = Container.newInstanceWithSetters(B.class);

        assertNull(fail);
    }

    @Test
    @DisplayName("Test new instance with constructor")
    void testNewInstanceWithConstructor()
    {
        A success = Container.newInstanceWithConstructor(A.class);

        assertNotNull(success);
        assertTrue(success.getB_public() instanceof D);

        B fail = Container.newInstanceWithConstructor(B.class);

        assertNull(fail);
    }

    @Test
    @DisplayName("Test new instance with attribute")
    void testNewInstanceWithAttribute()
    {
        A success = Container.newInstanceWithAttribute(A.class);

        assertNotNull(success);
        assertTrue(success.getB_public() instanceof D);
        assertTrue(success.getB_private() instanceof D);
        assertTrue(success.getB_protected() instanceof D);


        B fail = Container.newInstanceWithAttribute(B.class);

        assertNull(fail);
    }

    @Test
    @DisplayName("Test new instance with attribute get g")
    void testNewInstanceWithAttributeGetG()
    {
        A success = Container.newInstanceWithAttribute(A.class);

        assertNotNull(success);
        assertNotNull(success.b);
        assertNotNull(success.e);
        assertNotNull(success.e.f);
        assertNotNull(success.e.f.g);

        assertNotNull(success.b.getF());
        assertNotNull(success.b.getF().g);

    }


    @Test
    @DisplayName("Test new instance with constructor get g")
    void testNewInstanceWithConstructorGetG()
    {
        A success = Container.newInstanceWithConstructor(A.class);

        assertNotNull(success);
        assertNotNull(success.b);
        assertNotNull(success.e);
        assertNotNull(success.e.f);
        assertNotNull(success.e.f.g);

    }

    @Test
    @DisplayName("Test new instance with setter get g")
    void testNewInstanceWithSetterGetG()
    {
        A success = Container.newInstanceWithSetters(A.class);

        assertNotNull(success);
        assertNotNull(success.b);
        assertNotNull(success.e);
        assertNotNull(success.e.f);
        assertNotNull(success.e.f.g);

    }


    @Test
    @DisplayName("Test change dependency with constructor")
    void testChangeDependencyWithConstructor()
    {
        Container.register(B.class, C.class);

        A success = Container.newInstanceWithConstructor(A.class);

        assertTrue(success.b instanceof C);

        assertNotNull(success);
        assertNotNull(success.b);
        assertNotNull(success.e);
        assertNotNull(success.e.f);
        assertNotNull(success.e.f.g);

        assertNotNull(success.b.getF());
        assertNotNull(success.b.getF().g);
    }


    @Test
    @DisplayName("Test change dependency with attributes")
    void testChangeDependencyWithAttributes()
    {
        Container.register(B.class, C.class);

        A success = Container.newInstanceWithAttribute(A.class);

        assertTrue(success.b instanceof C);

        assertNotNull(success);
        assertNotNull(success.b);
        assertNotNull(success.e);
        assertNotNull(success.e.f);
        assertNotNull(success.e.f.g);

        assertNotNull(success.b.getF());
        assertNotNull(success.b.getF().g);
    }


    @Test
    @DisplayName("Test change dependency with setters")
    void testChangeDependencyWithSetters()
    {
        Container.register(B.class, C.class);

        A success = Container.newInstanceWithSetters(A.class);

        assertTrue(success.b instanceof C);

        assertNotNull(success);
        assertNotNull(success.b);
        assertNotNull(success.e);
        assertNotNull(success.e.f);
        assertNotNull(success.e.f.g);

        assertNotNull(success.b.getF());
        assertNotNull(success.b.getF().g);
    }

    @Test
    @DisplayName("Test register value")
    void testRegisterValue()
    {
        //Register value
        Container.registerValue("pi", 3.1415);
        assertEquals(1, Container._values.size());

        //Register null value
        Container.registerValue("Null", null);

        //Get existing value
        double pi = Container.getValue("pi");
        assertEquals(3.1415, pi, 0.00001);

        //Get non existing value
        assertNull(Container.getValue("Cheval"));
    }


}

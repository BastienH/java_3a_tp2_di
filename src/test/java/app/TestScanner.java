package app;

import app.classes.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestScanner {

    @Test
    @DisplayName("Test Scanner")
    void testScanner()
    {
        Scanner.scanClasses();

        assertEquals(4, Container._dependencies.size());
        assertEquals(E.class, Container._dependencies.get(E.class));
        assertEquals(F.class, Container._dependencies.get(F.class));
        assertEquals(G.class, Container._dependencies.get(G.class));
        assertEquals(D.class, Container._dependencies.get(B.class));
    }
}

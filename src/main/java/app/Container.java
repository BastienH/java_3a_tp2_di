package app;

import app.annotations.AttributeInjection;
import app.annotations.ConstructorInjection;
import app.annotations.SetterInjection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Stream;

public class Container{

    static Map<Class, Class> _dependencies = new Hashtable<Class, Class>();
    static Map<String, Object> _values = new Hashtable<String, Object>();

    static void register(Class mother, Class child)
    {
        if(mother.isInterface() && mother != child && !child.isInterface())
        {
            Stream stream = Arrays.stream(child.getInterfaces());

            Boolean isMother = stream.anyMatch(x -> x == mother);

            if(isMother)
                _dependencies.put(mother, child);
        }
        else if(child.isAssignableFrom(mother))
        {
            _dependencies.put(mother, child);
        }


    }

    //Setter injection
    static <T> T newInstanceWithSetters(Class<T> instance)
    {

        T t = null;

        try
        {
            t = instance.newInstance();

            Method[] methods = instance.getMethods();

            for(Method m : methods)
            {
                if(m.isAnnotationPresent(SetterInjection.class))
                {
                    Class dependency = _dependencies.get(m.getParameterTypes()[0]);

                    m.invoke(t, newInstanceWithSetters(dependency));

                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return t;
    }

    //Constructor injection
    static <T> T newInstanceWithConstructor(Class<T> instance)
    {
        T t = null;

        try
        {
            t = (T)instance.newInstance();

            Constructor[] constructors = instance.getDeclaredConstructors();

            for(Constructor c : constructors)
            {
                if(c.isAnnotationPresent(ConstructorInjection.class))
                {
                    List<Object> args = new ArrayList<>();
                    Class[] types = c.getParameterTypes();
                    for(Class value : types)
                    {
                        Class dependency = _dependencies.get(value);

                        args.add(newInstanceWithConstructor(dependency));

                    }

                    t = (T) c.newInstance(args.toArray());
                }
            }


        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return t;
    }

    //Attribute injection
    static <T> T newInstanceWithAttribute(Class<T> instance)
    {
        T t = null;

        try
        {
            t = (T)instance.newInstance();

            Field[] fields = instance.getDeclaredFields();

            for(Field f : fields)
            {
                if(f.isAnnotationPresent(AttributeInjection.class))
                {
                    f.setAccessible(true);

                    Class dependency = _dependencies.get(f.getType());

                    f.set(t, newInstanceWithAttribute(dependency));

                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return t;
    }

    //Values

    static void registerValue(String name, Object value )
    {
        if(value != null)
            _values.put(name, value);
    }

    static <T> T getValue(String name)
    {
        return (T)_values.get(name);
    }
}

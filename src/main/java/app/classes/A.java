package app.classes;

import app.annotations.*;

@ScanClass
public class A {

    @AttributeInjection()
    @LinkTo(dependency = E.class)
    public E e;

    @AttributeInjection()
    @LinkTo(dependency = D.class)
    public B b;

    @AttributeInjection()
    @LinkTo(dependency = D.class)
    private B b1;

    @AttributeInjection()
    @LinkTo(dependency = D.class)
    protected B b2;

    public A(){}

    @ConstructorInjection()
    public A(B b, E e)
    {
        this.b = b;
        this.e = e;
    }


    @SetterInjection()
    public void setE(E e){this.e = e;}

    @SetterInjection()
    public void setB(B b){this.b = b;}

    public B getB_public() {return b;}
    public B getB_private() {return b1;}
    public B getB_protected() {return b2;}

}

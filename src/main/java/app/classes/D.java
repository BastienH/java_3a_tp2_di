package app.classes;

import app.annotations.AttributeInjection;
import app.annotations.ConstructorInjection;
import app.annotations.LinkTo;
import app.annotations.SetterInjection;

public class D implements B{

    @AttributeInjection()
    @LinkTo(dependency = F.class)
    public F f;

    @SetterInjection()
    public void setF(F f) {
        this.f = f;
    }

    @Override
    public F getF()
    {
        return f;
    }

    public D(){}

    @ConstructorInjection()
    public D(F f)
    {
        this.f = f;
    }
}

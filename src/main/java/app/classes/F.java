package app.classes;

import app.annotations.*;

@ScanClass
public class F {

    @AttributeInjection()
    @LinkTo(dependency = G.class)
    public G g;

    public F(){}

    @ConstructorInjection()
    public F(G g)
    {
        this.g = g;
    }

    @SetterInjection()
    public void setG(G g){this.g = g;}


}

package app.classes;

import app.annotations.AttributeInjection;
import app.annotations.LinkTo;

public interface B {

    public void setF(F f);
    public F getF();
}


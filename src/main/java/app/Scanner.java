package app;

import app.annotations.AttributeInjection;
import app.annotations.LinkTo;
import app.annotations.ScanClass;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.util.Set;

public class Scanner {

    static void scanClasses(){

        Set<Class<?>> classes = new Reflections("app/classes").getTypesAnnotatedWith(ScanClass.class);

        for(Class c : classes)
        {
            Field[] attributes = c.getDeclaredFields();
            for(Field f : attributes)
            {
                if(f.isAnnotationPresent(LinkTo.class))
                {
                    LinkTo link = f.getAnnotation(LinkTo.class);
                    Container.register(f.getType(), link.dependency());
                }
            }
        }

    }
}
